# Microsoft Developer Studio Generated NMAKE File, Format Version 4.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

!IF "$(CFG)" == ""
CFG=Last_Sol - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to Last_Sol - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "Last_Sol - Win32 Release" && "$(CFG)" !=\
 "Last_Sol - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "Last_Sol.mak" CFG="Last_Sol - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Last_Sol - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "Last_Sol - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "Last_Sol - Win32 Debug"
F90=fl32.exe
RSC=rc.exe

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\Last_Sol.exe" "$(OUTDIR)\MDRELAX_old.mod"\
 "$(OUTDIR)\MDDYNAMSTW.MOD" "$(OUTDIR)\Last_Sol.bsc"

CLEAN : 
	-@erase ".\Release\MDRELAX_old.mod"
	-@erase ".\Release/MDTYPES.mod"
	-@erase ".\Release/MDPOTENTS.mod"
	-@erase ".\Release/MDSTRUCT.mod"
	-@erase ".\Release/MDCONSTS.mod"
	-@erase ".\Release/MDBOUNDS.mod"
	-@erase ".\Release\MDDYNAMSTW.MOD"
	-@erase ".\Release\Last_Sol.bsc"
	-@erase ".\Release\DINAM_STW.SBR"
	-@erase ".\Release\RELAX_SUB.SBR"
	-@erase ".\Release/MDRELAX.mod"
	-@erase ".\Release\STRUCT.SBR"
	-@erase ".\Release\RELAX_SUB_nik.sbr"
	-@erase ".\Release\TYPES.SBR"
	-@erase ".\Release\MATR_OPERS.SBR"
	-@erase ".\Release\RELAX.SBR"
	-@erase ".\Release\DINAM.SBR"
	-@erase ".\Release\BOUNDS.SBR"
	-@erase ".\Release\RELAX_nik.sbr"
	-@erase ".\Release\MAIN.SBR"
	-@erase ".\Release/MDMATRIX_TRANSFS.mod"
	-@erase ".\Release/MDDYNAM_TSF.mod"
	-@erase ".\Release/MDDYNAM_STW.mod"
	-@erase ".\Release/MDRAMAN_SPECTRES.mod"
	-@erase ".\Release\DINAM_TSF.SBR"
	-@erase ".\Release\CONSTS.SBR"
	-@erase ".\Release\RAMAN.SBR"
	-@erase ".\Release\POTENTIALS.SBR"
	-@erase ".\Release\Last_Sol.exe"
	-@erase ".\Release\MAIN.OBJ"
	-@erase ".\Release\DINAM_TSF.OBJ"
	-@erase ".\Release\CONSTS.OBJ"
	-@erase ".\Release\RAMAN.OBJ"
	-@erase ".\Release\POTENTIALS.OBJ"
	-@erase ".\Release\DINAM_STW.OBJ"
	-@erase ".\Release\RELAX_SUB.OBJ"
	-@erase ".\Release\STRUCT.OBJ"
	-@erase ".\Release\RELAX_SUB_nik.obj"
	-@erase ".\Release\TYPES.OBJ"
	-@erase ".\Release\MATR_OPERS.OBJ"
	-@erase ".\Release\RELAX.OBJ"
	-@erase ".\Release\DINAM.OBJ"
	-@erase ".\Release\BOUNDS.OBJ"
	-@erase ".\Release\RELAX_nik.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "Release/" /c /nologo
# ADD F90 /Ox /FR /I "Release/" /c /nologo
F90_PROJ=/Ox /FR"Release/" /I "Release/" /c /nologo /Fo"Release/" 
F90_OBJS=.\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/Last_Sol.bsc" 
BSC32_SBRS= \
	"$(INTDIR)/DINAM_STW.SBR" \
	"$(INTDIR)/RELAX_SUB.SBR" \
	"$(INTDIR)/STRUCT.SBR" \
	"$(INTDIR)/RELAX_SUB_nik.sbr" \
	"$(INTDIR)/TYPES.SBR" \
	"$(INTDIR)/MATR_OPERS.SBR" \
	"$(INTDIR)/RELAX.SBR" \
	"$(INTDIR)/DINAM.SBR" \
	"$(INTDIR)/BOUNDS.SBR" \
	"$(INTDIR)/RELAX_nik.sbr" \
	"$(INTDIR)/MAIN.SBR" \
	"$(INTDIR)/DINAM_TSF.SBR" \
	"$(INTDIR)/CONSTS.SBR" \
	"$(INTDIR)/RAMAN.SBR" \
	"$(INTDIR)/POTENTIALS.SBR"

"$(OUTDIR)\Last_Sol.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/Last_Sol.pdb" /machine:I386 /out:"$(OUTDIR)/Last_Sol.exe" 
LINK32_OBJS= \
	"$(INTDIR)/MAIN.OBJ" \
	"$(INTDIR)/DINAM_TSF.OBJ" \
	"$(INTDIR)/CONSTS.OBJ" \
	"$(INTDIR)/RAMAN.OBJ" \
	"$(INTDIR)/POTENTIALS.OBJ" \
	"$(INTDIR)/DINAM_STW.OBJ" \
	"$(INTDIR)/RELAX_SUB.OBJ" \
	"$(INTDIR)/STRUCT.OBJ" \
	"$(INTDIR)/RELAX_SUB_nik.obj" \
	"$(INTDIR)/TYPES.OBJ" \
	"$(INTDIR)/MATR_OPERS.OBJ" \
	"$(INTDIR)/RELAX.OBJ" \
	"$(INTDIR)/DINAM.OBJ" \
	"$(INTDIR)/BOUNDS.OBJ" \
	"$(INTDIR)/RELAX_nik.obj"

"$(OUTDIR)\Last_Sol.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\Last_Sol.exe" "$(OUTDIR)\MDRELAX_old.mod"\
 "$(OUTDIR)\MDDYNAMSTW.MOD" "$(OUTDIR)\Last_Sol.bsc"

CLEAN : 
	-@erase ".\Debug\MDRELAX_old.mod"
	-@erase ".\Debug/MDTYPES.mod"
	-@erase ".\Debug/MDPOTENTS.mod"
	-@erase ".\Debug/MDSTRUCT.mod"
	-@erase ".\Debug/MDCONSTS.mod"
	-@erase ".\Debug/MDBOUNDS.mod"
	-@erase ".\Debug\MDDYNAMSTW.MOD"
	-@erase ".\Debug\Last_Sol.bsc"
	-@erase ".\Debug\RELAX.SBR"
	-@erase ".\Debug\DINAM_TSF.SBR"
	-@erase ".\Debug\POTENTIALS.SBR"
	-@erase ".\Debug\RELAX_SUB_nik.sbr"
	-@erase ".\Debug/MDRELAX.mod"
	-@erase ".\Debug\DINAM.SBR"
	-@erase ".\Debug\MATR_OPERS.SBR"
	-@erase ".\Debug\MAIN.SBR"
	-@erase ".\Debug/MDMATRIX_TRANSFS.mod"
	-@erase ".\Debug/MDDYNAM_TSF.mod"
	-@erase ".\Debug/MDDYNAM_STW.mod"
	-@erase ".\Debug/MDRAMAN_SPECTRES.mod"
	-@erase ".\Debug\BOUNDS.SBR"
	-@erase ".\Debug\DINAM_STW.SBR"
	-@erase ".\Debug\TYPES.SBR"
	-@erase ".\Debug\RELAX_SUB.SBR"
	-@erase ".\Debug\RAMAN.SBR"
	-@erase ".\Debug\STRUCT.SBR"
	-@erase ".\Debug\CONSTS.SBR"
	-@erase ".\Debug\RELAX_nik.sbr"
	-@erase ".\Debug\Last_Sol.exe"
	-@erase ".\Debug\CONSTS.OBJ"
	-@erase ".\Debug\RELAX_nik.obj"
	-@erase ".\Debug\RELAX.OBJ"
	-@erase ".\Debug\DINAM_TSF.OBJ"
	-@erase ".\Debug\POTENTIALS.OBJ"
	-@erase ".\Debug\RELAX_SUB_nik.obj"
	-@erase ".\Debug\DINAM.OBJ"
	-@erase ".\Debug\MATR_OPERS.OBJ"
	-@erase ".\Debug\MAIN.OBJ"
	-@erase ".\Debug\BOUNDS.OBJ"
	-@erase ".\Debug\DINAM_STW.OBJ"
	-@erase ".\Debug\TYPES.OBJ"
	-@erase ".\Debug\RELAX_SUB.OBJ"
	-@erase ".\Debug\RAMAN.OBJ"
	-@erase ".\Debug\STRUCT.OBJ"
	-@erase ".\Debug\Last_Sol.ilk"
	-@erase ".\Debug\Last_Sol.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "Debug/" /c /nologo
# ADD F90 /FR /Zi /I "Debug/" /c /nologo
F90_PROJ=/FR"Debug/" /Zi /I "Debug/" /c /nologo /Fo"Debug/"\
 /Fd"Debug/Last_Sol.pdb" 
F90_OBJS=.\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/Last_Sol.bsc" 
BSC32_SBRS= \
	"$(INTDIR)/RELAX.SBR" \
	"$(INTDIR)/DINAM_TSF.SBR" \
	"$(INTDIR)/POTENTIALS.SBR" \
	"$(INTDIR)/RELAX_SUB_nik.sbr" \
	"$(INTDIR)/DINAM.SBR" \
	"$(INTDIR)/MATR_OPERS.SBR" \
	"$(INTDIR)/MAIN.SBR" \
	"$(INTDIR)/BOUNDS.SBR" \
	"$(INTDIR)/DINAM_STW.SBR" \
	"$(INTDIR)/TYPES.SBR" \
	"$(INTDIR)/RELAX_SUB.SBR" \
	"$(INTDIR)/RAMAN.SBR" \
	"$(INTDIR)/STRUCT.SBR" \
	"$(INTDIR)/CONSTS.SBR" \
	"$(INTDIR)/RELAX_nik.sbr"

"$(OUTDIR)\Last_Sol.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/Last_Sol.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/Last_Sol.exe" 
LINK32_OBJS= \
	"$(INTDIR)/CONSTS.OBJ" \
	"$(INTDIR)/RELAX_nik.obj" \
	"$(INTDIR)/RELAX.OBJ" \
	"$(INTDIR)/DINAM_TSF.OBJ" \
	"$(INTDIR)/POTENTIALS.OBJ" \
	"$(INTDIR)/RELAX_SUB_nik.obj" \
	"$(INTDIR)/DINAM.OBJ" \
	"$(INTDIR)/MATR_OPERS.OBJ" \
	"$(INTDIR)/MAIN.OBJ" \
	"$(INTDIR)/BOUNDS.OBJ" \
	"$(INTDIR)/DINAM_STW.OBJ" \
	"$(INTDIR)/TYPES.OBJ" \
	"$(INTDIR)/RELAX_SUB.OBJ" \
	"$(INTDIR)/RAMAN.OBJ" \
	"$(INTDIR)/STRUCT.OBJ"

"$(OUTDIR)\Last_Sol.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

################################################################################
# Begin Target

# Name "Last_Sol - Win32 Release"
# Name "Last_Sol - Win32 Debug"

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\TYPES.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

F90_MODOUT=\
	"MDTYPES"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\TYPES.OBJ" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\TYPES.SBR" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\mdtypes.mod" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

F90_MODOUT=\
	"MDTYPES"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\TYPES.OBJ" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\TYPES.SBR" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\mdtypes.mod" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\STRUCT.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_STRUC=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDBOUNDS.mod"\
	
F90_MODOUT=\
	"MDSTRUCT"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\STRUCT.OBJ" : $(SOURCE) $(DEP_F90_STRUC) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\STRUCT.SBR" : $(SOURCE) $(DEP_F90_STRUC) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mdstruct.mod" : $(SOURCE) $(DEP_F90_STRUC) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_STRUC=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	
F90_MODOUT=\
	"MDSTRUCT"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\STRUCT.OBJ" : $(SOURCE) $(DEP_F90_STRUC) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\STRUCT.SBR" : $(SOURCE) $(DEP_F90_STRUC) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mdstruct.mod" : $(SOURCE) $(DEP_F90_STRUC) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RELAX_SUB.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_RELAX=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	".\Release/MDRELAX.mod"\
	

"$(INTDIR)\RELAX_SUB.OBJ" : $(SOURCE) $(DEP_F90_RELAX) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod" "$(INTDIR)\mdrelax.mod"

"$(INTDIR)\RELAX_SUB.SBR" : $(SOURCE) $(DEP_F90_RELAX) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod" "$(INTDIR)\mdrelax.mod"


!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_RELAX=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	".\Debug/MDRELAX.mod"\
	

"$(INTDIR)\RELAX_SUB.OBJ" : $(SOURCE) $(DEP_F90_RELAX) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod" "$(INTDIR)\mdrelax.mod"

"$(INTDIR)\RELAX_SUB.SBR" : $(SOURCE) $(DEP_F90_RELAX) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod" "$(INTDIR)\mdrelax.mod"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RELAX.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_RELAX_=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDPOTENTS.mod"\
	".\Release/MDBOUNDS.mod"\
	
F90_MODOUT=\
	"MDRELAX_old"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\RELAX.OBJ" : $(SOURCE) $(DEP_F90_RELAX_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdpotents.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\RELAX.SBR" : $(SOURCE) $(DEP_F90_RELAX_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdpotents.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\MDRELAX_old.mod" : $(SOURCE) $(DEP_F90_RELAX_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdpotents.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_RELAX_=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDPOTENTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	
F90_MODOUT=\
	"MDRELAX_old"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\RELAX.OBJ" : $(SOURCE) $(DEP_F90_RELAX_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdpotents.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\RELAX.SBR" : $(SOURCE) $(DEP_F90_RELAX_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdpotents.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\MDRELAX_old.mod" : $(SOURCE) $(DEP_F90_RELAX_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdpotents.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\POTENTIALS.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_POTEN=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDPOTENTS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\POTENTIALS.OBJ" : $(SOURCE) $(DEP_F90_POTEN) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\POTENTIALS.SBR" : $(SOURCE) $(DEP_F90_POTEN) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\mdpotents.mod" : $(SOURCE) $(DEP_F90_POTEN) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_POTEN=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDPOTENTS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\POTENTIALS.OBJ" : $(SOURCE) $(DEP_F90_POTEN) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\POTENTIALS.SBR" : $(SOURCE) $(DEP_F90_POTEN) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\mdpotents.mod" : $(SOURCE) $(DEP_F90_POTEN) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MATR_OPERS.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_MATR_=\
	".\Release/MDCONSTS.mod"\
	".\Release/MDTYPES.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDMATRIX_TRANSFS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\MATR_OPERS.OBJ" : $(SOURCE) $(DEP_F90_MATR_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\MATR_OPERS.SBR" : $(SOURCE) $(DEP_F90_MATR_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mdmatrix_transfs.mod" : $(SOURCE) $(DEP_F90_MATR_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_MATR_=\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDMATRIX_TRANSFS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\MATR_OPERS.OBJ" : $(SOURCE) $(DEP_F90_MATR_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\MATR_OPERS.SBR" : $(SOURCE) $(DEP_F90_MATR_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mdmatrix_transfs.mod" : $(SOURCE) $(DEP_F90_MATR_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MAIN.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_MAIN_=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDSTRUCT.mod"\
	".\Release/MDPOTENTS.mod"\
	".\Release/MDDYNAM_TSF.mod"\
	".\Release/MDDYNAM_STW.mod"\
	".\Release/MDMATRIX_TRANSFS.mod"\
	".\Release/MDRAMAN_SPECTRES.mod"\
	

"$(INTDIR)\MAIN.OBJ" : $(SOURCE) $(DEP_F90_MAIN_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdmatrix_transfs.mod" "$(INTDIR)\mddynam_tsf.mod"\
 "$(INTDIR)\mddynam_stw.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdraman_spectres.mod"

"$(INTDIR)\MAIN.SBR" : $(SOURCE) $(DEP_F90_MAIN_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdmatrix_transfs.mod" "$(INTDIR)\mddynam_tsf.mod"\
 "$(INTDIR)\mddynam_stw.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdraman_spectres.mod"


!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_MAIN_=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDSTRUCT.mod"\
	".\Debug/MDPOTENTS.mod"\
	".\Debug/MDDYNAM_TSF.mod"\
	".\Debug/MDDYNAM_STW.mod"\
	".\Debug/MDMATRIX_TRANSFS.mod"\
	".\Debug/MDRAMAN_SPECTRES.mod"\
	

"$(INTDIR)\MAIN.OBJ" : $(SOURCE) $(DEP_F90_MAIN_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdmatrix_transfs.mod" "$(INTDIR)\mddynam_tsf.mod"\
 "$(INTDIR)\mddynam_stw.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdraman_spectres.mod"

"$(INTDIR)\MAIN.SBR" : $(SOURCE) $(DEP_F90_MAIN_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdmatrix_transfs.mod" "$(INTDIR)\mddynam_tsf.mod"\
 "$(INTDIR)\mddynam_stw.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdraman_spectres.mod"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DINAM_TSF.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

NODEP_F90_DINAM=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDDYNAM_TSF"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\DINAM_TSF.OBJ" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\DINAM_TSF.SBR" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mddynam_tsf.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_DINAM=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDDYNAM_TSF"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\DINAM_TSF.OBJ" : $(SOURCE) $(DEP_F90_DINAM) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\DINAM_TSF.SBR" : $(SOURCE) $(DEP_F90_DINAM) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mddynam_tsf.mod" : $(SOURCE) $(DEP_F90_DINAM) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DINAM_STW.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

NODEP_F90_DINAM_=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDDYNAM_STW"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\DINAM_STW.OBJ" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\DINAM_STW.SBR" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mddynam_stw.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_DINAM_=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDDYNAM_STW"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\DINAM_STW.OBJ" : $(SOURCE) $(DEP_F90_DINAM_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\DINAM_STW.SBR" : $(SOURCE) $(DEP_F90_DINAM_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mddynam_stw.mod" : $(SOURCE) $(DEP_F90_DINAM_) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DINAM.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

NODEP_F90_DINAM_F=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDDYNAMSTW"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\DINAM.OBJ" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\DINAM.SBR" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\MDDYNAMSTW.MOD" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_DINAM_F=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	
F90_MODOUT=\
	"MDDYNAMSTW"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\DINAM.OBJ" : $(SOURCE) $(DEP_F90_DINAM_F) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\DINAM.SBR" : $(SOURCE) $(DEP_F90_DINAM_F) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\MDDYNAMSTW.MOD" : $(SOURCE) $(DEP_F90_DINAM_F) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdconsts.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\CONSTS.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

F90_MODOUT=\
	"MDCONSTS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\CONSTS.OBJ" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\CONSTS.SBR" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\mdconsts.mod" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

F90_MODOUT=\
	"MDCONSTS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\CONSTS.OBJ" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\CONSTS.SBR" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\mdconsts.mod" : $(SOURCE) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\BOUNDS.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

NODEP_F90_BOUND=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	
F90_MODOUT=\
	"MDBOUNDS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\BOUNDS.OBJ" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\BOUNDS.SBR" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\mdbounds.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\mdtypes.mod"\
 "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_BOUND=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	
F90_MODOUT=\
	"MDBOUNDS"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\BOUNDS.OBJ" : $(SOURCE) $(DEP_F90_BOUND) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\BOUNDS.SBR" : $(SOURCE) $(DEP_F90_BOUND) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

"$(INTDIR)\mdbounds.mod" : $(SOURCE) $(DEP_F90_BOUND) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_TYPES.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_PARAMS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_NUMS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_NEIBRS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_LEVELS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_LAYS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_COORDS.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX_COORDS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RELAX.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RAMAN_SPECTRE_si_cub_prdc.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_RAMAN_SPECTRE.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_PHONON_SPECTRE.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_OWN_VECTS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_DINAM_MATRIX.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_BONDS_LENGTHS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_BOND_ENERGIES.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_ANGLES.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=".\Проверка 10_02_2010.xls"

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rez_1106.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RAMAN.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_RAMAN=\
	".\Release/MDCONSTS.mod"\
	".\Release/MDTYPES.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	".\Release/MDMATRIX_TRANSFS.mod"\
	
F90_MODOUT=\
	"MDRAMAN_SPECTRES"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\RAMAN.OBJ" : $(SOURCE) $(DEP_F90_RAMAN) "$(INTDIR)"\
 "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdmatrix_transfs.mod"
   $(BuildCmds)

"$(INTDIR)\RAMAN.SBR" : $(SOURCE) $(DEP_F90_RAMAN) "$(INTDIR)"\
 "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdmatrix_transfs.mod"
   $(BuildCmds)

"$(INTDIR)\mdraman_spectres.mod" : $(SOURCE) $(DEP_F90_RAMAN) "$(INTDIR)"\
 "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdmatrix_transfs.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_RAMAN=\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	".\Debug/MDMATRIX_TRANSFS.mod"\
	
F90_MODOUT=\
	"MDRAMAN_SPECTRES"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\RAMAN.OBJ" : $(SOURCE) $(DEP_F90_RAMAN) "$(INTDIR)"\
 "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdmatrix_transfs.mod"
   $(BuildCmds)

"$(INTDIR)\RAMAN.SBR" : $(SOURCE) $(DEP_F90_RAMAN) "$(INTDIR)"\
 "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdmatrix_transfs.mod"
   $(BuildCmds)

"$(INTDIR)\mdraman_spectres.mod" : $(SOURCE) $(DEP_F90_RAMAN) "$(INTDIR)"\
 "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdmatrix_transfs.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_OTLAD.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\_IPR.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rez_1506.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rez_1706.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rez_2106.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rez_2306.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Rez_2406.xls

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RELAX_nik.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_RELAX_N=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDPOTENTS.mod"\
	".\Release/MDBOUNDS.mod"\
	
F90_MODOUT=\
	"MDRELAX"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\RELAX_nik.obj" : $(SOURCE) $(DEP_F90_RELAX_N) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\RELAX_nik.sbr" : $(SOURCE) $(DEP_F90_RELAX_N) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mdrelax.mod" : $(SOURCE) $(DEP_F90_RELAX_N) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_RELAX_N=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDPOTENTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	
F90_MODOUT=\
	"MDRELAX"


BuildCmds= \
	$(F90) $(F90_PROJ) $(SOURCE) \
	

"$(INTDIR)\RELAX_nik.obj" : $(SOURCE) $(DEP_F90_RELAX_N) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\RELAX_nik.sbr" : $(SOURCE) $(DEP_F90_RELAX_N) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

"$(INTDIR)\mdrelax.mod" : $(SOURCE) $(DEP_F90_RELAX_N) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdpotents.mod"\
 "$(INTDIR)\mdbounds.mod"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RELAX_SUB_nik.FOR

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

DEP_F90_RELAX_S=\
	".\Release/MDTYPES.mod"\
	".\Release/MDCONSTS.mod"\
	".\Release/MDBOUNDS.mod"\
	".\Release/MDSTRUCT.mod"\
	".\Release/MDRELAX.mod"\
	

"$(INTDIR)\RELAX_SUB_nik.obj" : $(SOURCE) $(DEP_F90_RELAX_S) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdrelax.mod"

"$(INTDIR)\RELAX_SUB_nik.sbr" : $(SOURCE) $(DEP_F90_RELAX_S) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdrelax.mod"


!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

DEP_F90_RELAX_S=\
	".\Debug/MDTYPES.mod"\
	".\Debug/MDCONSTS.mod"\
	".\Debug/MDBOUNDS.mod"\
	".\Debug/MDSTRUCT.mod"\
	".\Debug/MDRELAX.mod"\
	

"$(INTDIR)\RELAX_SUB_nik.obj" : $(SOURCE) $(DEP_F90_RELAX_S) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdrelax.mod"

"$(INTDIR)\RELAX_SUB_nik.sbr" : $(SOURCE) $(DEP_F90_RELAX_S) "$(INTDIR)"\
 "$(INTDIR)\mdtypes.mod" "$(INTDIR)\mdconsts.mod" "$(INTDIR)\mdbounds.mod"\
 "$(INTDIR)\mdstruct.mod" "$(INTDIR)\mdrelax.mod"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=".\Emin_v_alfa_x=0.5.xls"

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\COORDS.TXT

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=".\Emin_v_alfa_x=0.3   24.04.2013.xls"

!IF  "$(CFG)" == "Last_Sol - Win32 Release"

!ELSEIF  "$(CFG)" == "Last_Sol - Win32 Debug"

!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
