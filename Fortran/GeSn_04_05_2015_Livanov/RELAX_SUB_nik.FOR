	!-------------------------------------------------------------------------------------------------------------------------
	SUBROUTINE RELAX(STRUCT,POTENT_TYPE,DTIME_Rel,MAX_STEPS,PLT)

	USE MDTYPES
	USE MDCONSTS
	USE MDBOUNDS
	USE MDSTRUCT 
	USE MDRELAX	

	TYPE(T_STRUCT),INTENT(INOUT)::STRUCT
	REAL*8 DTIME_Rel
	INTEGER*4 MAX_STEPS
	INTEGER*2 POTENT_TYPE
	REAL*8 PLT

	REAL*8 E0,EK1,EP1,EK2,EP2
	REAL*8 DELTA,TIME_Rel
	INTEGER*4 STEP,SK
	REAL*8,POINTER,DIMENSION(:)::EK,EP
	INTEGER*4 CONT


	CALL CREATE_SYSTEM(STRUCT,DTIME_Rel)
	
	E0=ENERGY(STRUCT,POTENT_TYPE,SIZE(STRUCT%ATS))

	WRITE(*,*) 'E0=',E0
	! -------------------------------------------------------------------

	EK1=0.0
	EP1=E0	
      STEP=1
	TIME_Rel=0.0
	SK=0

	ALLOCATE(EK(1))
	ALLOCATE(EP(1))

	EK(1)=0.0
	EP(1)=E0
	DELTA=0.005	

	!----------------��������������� ����������----------------------------
	DO WHILE(STEP<MAX_STEPS)

	CALL SAVE_COORDS
	

	IF(POTENT_TYPE.EQ.P_TSF) THEN	 
	CALL ITER(TERSF_I)
	EP2=ENERGY(RSTR,P_TSF,SIZE(RSTR%ATS))
	ELSE 
	CALL ITER(STW_I)
	EP2=ENERGY(RSTR,P_STW,SIZE(RSTR%ATS))
	END IF

	EK2=EKIN(RSTR)

	WRITE(*,*)
	WRITE(*,*) 'STEP=',STEP
	WRITE(10,*) 
	WRITE(10,*) 'STEP=',STEP
	WRITE(*,*) 'EK2=',EK2,'         DEK=',EK2-EK1
	WRITE(*,*) 'EP2=',EP2,'         DEP=',EP2-EP1
	E1=EK1+EP1
	E2=EK2+EP2
!	WRITE(*,*)    'E1=',E1,'         E2=',E2
	WRITE(10,*) 'DEP=',EP1-EP2,'     DEK=',EK2-EK1
!	WRITE(*,*) 'E0=',E0
	WRITE(10,*) 'EP2=',EP2,'       Ek2=',Ek2,'  TIME_Rel=',TIME_Rel

	IF10:IF(ABS((E2-E0)/E0).GT.DELTA.OR.(EP2-EP1)*(EK1-EK2).LT.0) THEN

	!��������� ������� ����� ���� - ������� ����������
	IF2:IF(ABS(EK1-EK2).LT.1E-6.OR.ABS(EP1-EP2).LT.1E-6) THEN

	WRITE(*,*) '-> ENERGY MINIMUM'

!	IF(ABS((EP2-E0)/E0).LT.0.001) EXIT
      IF3:IF(ABS((EP2-E0)/E0).LT.0.001) THEN 
	
	WRITE(*,*) 'DELTA E <0.001. CONTINUE RELAX? 0-NO,1-YES'
	READ(*,*) CONT
	IF(CONT.EQ.0) EXIT      

	END IF IF3



	CALL NULL_SPEEDS
	ALLOCATE(EK(1))
	ALLOCATE(EP(1))
	EK1=0.0
	E0=EP2
	EP1=EP2

	EK(1)=EK1
	EP(1)=EP2
	IF(DT.LT.1E-3) DT=1E-3

	ELSE  		  !IF2

	WRITE(*,*) 'ENERGY IS NOT KEPT'
	DT=DT*0.5
	SK=0	

	IF4:IF(DT.LT.1E-10) THEN

        WRITE(*,*) 'DT<1E-10'
	    DT=0.001
	    WRITE(*,*) 'CHANGE DELTA? 0-NO,1-YES'
	    READ(*,*) REL
	 
	    IF5:IF(REL.NE.0) THEN
	        WRITE(*,*) 'INPUT DELTA'
	        READ(*,*) DELTA
        END IF IF5 

	END IF	IF4

	WRITE(*,*) 'NEW DT=',DT	
	
	CALL LOAD_COORDS

	END IF	IF2


	ELSE			  !  IF10

	STEP=STEP+1

	IF(MOD(STEP,10).EQ.0) THEN

	CALL SET_ENVIRS(RSTR,2.0*PLT*SQRT(3.0)/4.0)
	
	WRITE(*,*) 'ENVIRONMENT WAS FOUNDED'

	END IF

	TIME_Rel=TIME_Rel+DT
	WRITE(*,*) 'TIME_Rel=',TIME_Rel
	EK1=EK2
	EP1=EP2
	SK=SK+1

	IF(SK.GT.10.AND.DT.LE.0.0051) THEN
	SK=0
	DT=2.0*DT
	WRITE(*,*) 'NEW DT=',DT	 
	END IF

	NM=ADD(EP,EP2)
	NM=ADD(EK,EK2)

	!��������� ���������
	!I6:IF(NM.GT.3.AND.EK(NM-2).LT.EK(NM-1).AND.EK(NM).LT.EK(NM-1))THEN	
	I6:IF(NM.GT.3)THEN	
    	
	    if(EK(NM-2).LT.EK(NM-1).AND.EK(NM).LT.EK(NM-1)) then

	        IF7:IF(ABS((EP2-E0)/E0).LT.0.001) THEN 
        	
	        WRITE(*,*) 'DELTA E <0.001. CONTINUE RELAX? 0-NO,1-YES'
	        READ(*,*) CONT
	        IF(CONT.EQ.0) EXIT      

	        END IF IF7

	        CALL NULL_SPEEDS
	        E0=EP2
	        WRITE(*,*) '*************SPEEDS ARE NULL***************'

	        EP1=EP2
	        EK1=0.0

	        ALLOCATE(EP(1))
	        ALLOCATE(EK(1))
	        EK(1)=EK1
	        EP(1)=EP1

	        IF(DT.LT.1E-3) DT=1E-3

		endif
       END IF 	I6


	END IF IF10	

	END DO

	DEALLOCATE(EK)
	DEALLOCATE(EP) 	
	WRITE(1,*) ' Vsego shagov: ', step
	WRITE(1,*) ' Time all: ', TIME_Rel
	WRITE(1,*) ' Epot_end = : ', Ep2
 
	WRITE (*,*) 'POSLE CIKLA'
	CALL GET_RESULT(STRUCT)
	WRITE (*,*) 'POSLE GET_RESULT'	 

	CALL DESTROY_SYSTEM
	WRITE (*,*) 'POSLE DESTROY SYSTEM'

	END SUBROUTINE



	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!-----------���������� � �������� ������������------
	SUBROUTINE RELAX_TEMP(STRUCT,POTENT_TYPE,DTIME_Rel,MAX_STEPS,PLT)

	USE MDTYPES
	USE MDCONSTS
	USE MDBOUNDS
	USE MDSTRUCT 
	USE MDRELAX	
	USE MSFLIB

	TYPE(T_STRUCT),INTENT(INOUT)::STRUCT
	REAL*8 DTIME_Rel , DRmax,beta
	REAL*4  ran
	INTEGER*4 MAX_STEPS	, ztr
	INTEGER*2 POTENT_TYPE
	REAL*8 PLT

	REAL*8 E0,EK1,EP1,EK2,EP2,EKravn
	REAL*8 DELTA,TIME_Rel,kbol,EK,EP
	INTEGER*4 STEP,SK

!	REAL*8,POINTER,DIMENSION(:)::EK,EP
!	INTEGER*4 CONT

!	WRITE(*,*) ' TEMPERATURA v grad Kelvina ?'
!	READ(*,*) TK

!	CALL CREATE_SYSTEM(STRUCT,DTIME_Rel)		 �������� ����
	
	E0=ENERGY(STRUCT,POTENT_TYPE,SIZE(STRUCT%ATS))

	WRITE(*,*) 'E0=',E0
	WRITE(10,*) 'E0=',E0

	! -------------------------------------------------------------------
	N_MOVES= STRUCT%NATOMS
	kbol= 8.6142E-5
	EKravn =3./2*N_MOVES*kbol*TK
!	write(*,*) 'EKravn = ',EKravn
!	write(10,*) ' T = ',TK,'  EKravn = ',EKravn

!	Vmax=0.1  ! (��� 273 � �������� 0.01 - 0.05)
!	DRmax= 0.005  !  nm ( �������� 0.0 ? PLT)
!	write(*,*) 'zatravka dlja zadanija smeshenija - 7 znaka'
!	read(*,*) ztrv 
	
!      CALL SEED(ztr)
!      CALL RANDOM(ran)

!	write(*,*) ran		_______________
!	DO I=1,N_MOVES
!	CALL RANDOM(ran)
!  	  V(i)%x=Vmax*(2*ran-1)
!		write(10,*) ' V(i)%x = ',V(i)%x
!	CALL RANDOM(ran)
!	write(10,*) ran
!	  V(i)%y=Vmax*(2*ran-1)
!	CALL RANDOM(ran)
!	write(10,*) ran
!	  V(i)%z=Vmax*(2*ran-1)
		 !������� ��������� ��������

!	CALL RANDOM(ran)
!	write(10,*)  STRUCT%ATS(I)%X
!      STRUCT%ATS(I)%X=STRUCT%ATS(I)%X+DRmax*(2.0*ran-1.0)
!	write(10,*)  STRUCT%ATS(I)%X

!	CALL RANDOM(ran)
!      STRUCT%ATS(I)%y=STRUCT%ATS(I)%y+DRmax*(2.0*ran-1.0)
!	write(10,*)  DRmax*(2.0*ran-1.0)

!	CALL RANDOM(ran)
!      STRUCT%ATS(I)%z=STRUCT%ATS(I)%z+DRmax*(2.0*ran-1.0)
!	write(10,*)  DRmax*(2.0*ran-1.0)

	!������� ���������� ��������

!    	END DO

	E0=ENERGY(STRUCT,POTENT_TYPE,SIZE(STRUCT%ATS))
	WRITE(*,*) 'E0=',E0
	WRITE(10,*) 'E0=',E0

	
	CALL CREATE_SYSTEM(STRUCT,DTIME_Rel)		  !������� ����


			EK1=EKIN(STRUCT)
 	WRITE(*,*) 'EK1=',EK1
	WRITE(10,*) 'EK1=',EK1



	EP1=E0	
      STEP=1
	TIME_Rel=0.0
	SK=0

!	ALLOCATE(EK(1))
!	ALLOCATE(EP(1))

	EK=0.0
	EP=E0
	DELTA=0.005		 ! �������� ����� �� x, y,z � ��

	!----------------��������������� ����������----------------------------
	DO WHILE(STEP.LE.MAX_STEPS)
	if(step.eq.100) dt=dt/2
	if(step.eq.150) dt=dt/2
!	if(step.eq.300) dt=dt/2		  ! �� 1000 �����

	CALL SAVE_COORDS				   ! ���� ���������������
	

	IF(POTENT_TYPE.EQ.P_TSF) THEN	 
	CALL ITER(TERSF_I)
	EP2=ENERGY(RSTR,P_TSF,SIZE(RSTR%ATS))
	ELSE 
	CALL ITER(STW_I)
	EP2=ENERGY(RSTR,P_STW,SIZE(RSTR%ATS))
	END IF

	EK2=EKIN(RSTR)

	WRITE(*,*)
	WRITE(*,*) 'STEP=',STEP
	WRITE(10,*) 
	WRITE(10,*) 'STEP=',STEP
	WRITE(*,*) 'EK2=',EK2,'         DEK=',EK2-EK1
	WRITE(*,*) 'EP2=',EP2,'         DEP=',EP2-EP1
	E1=EK1+EP1
	E2=EK2+EP2
!	WRITE(*,*)    'E1=',E1,'         E2=',E2	
!	WRITE(*,*) 'E0=',E0
	WRITE(10,*) 'EP2=',EP2,'       Ek2=',Ek2,'  TIME_Rel=',TIME_Rel
	WRITE(10,*) 'DEP=',EP1-EP2,'     DEK=',EK2-EK1
!	IF10:IF(ABS((E2-E0)/E0).GT.DELTA.OR.(EP2-EP1)*(EK1-EK2).LT.0) THEN

	!��������� ������� ����� ���� - ������� ����������
!	IF2:IF(ABS(EK1-EK2).LT.1E-6.OR.ABS(EP1-EP2).LT.1E-6) THEN

!	WRITE(*,*) '-> ENERGY MINIMUM'

!	IF(ABS((EP2-E0)/E0).LT.0.001) EXIT
!      IF3:IF(ABS((EP2-E0)/E0).LT.0.001) THEN 
	
!	WRITE(*,*) 'DELTA E <0.001. CONTINUE RELAX? 0-NO,1-YES'
!	READ(*,*) CONT
!	IF(CONT.EQ.0) EXIT      

!	END IF IF3
	if(step/20*20.eq.step) then 

!	if(step/10*10.eq.step) then 
	beta =0.5	       !   beta=sqrt(EKravn/Ek2)
	write(10,*) ' beta =',beta
 	write(*,*) ' beta =',beta


	DO I=1,N_MOVES

  	  V(i)%x=V(i)%x*beta
	  V(i)%y=V(i)%y*beta
	  V(i)%z=V(i)%z*beta		 !�������������� ���������
    	END DO
	end if

!	CALL NULL_SPEEDS
!	ALLOCATE(EK(1))
!	ALLOCATE(EP(1))
!	EK1=0.0
!	E0=EP2
!	EP1=EP2
 !
!	EK(1)=EK1
!	EP(1)=EP2
!	IF(DT.LT.1E-3) DT=1E-3
 !
!	ELSE  		  !IF2
 !
!	WRITE(*,*) 'ENERGY IS NOT KEPT'
!	DT=DT*0.5
!	SK=0	
 !
!	IF4:IF(DT.LT.1E-10) THEN
 !
  !      WRITE(*,*) 'DT<1E-10'
!	    DT=0.001
!	    WRITE(*,*) 'CHANGE DELTA? 0-NO,1-YES'
!	    READ(*,*) REL
!	 
!	    IF5:IF(REL.NE.0) THEN
!	        WRITE(*,*) 'INPUT DELTA'
!	        READ(*,*) DELTA
 !       END IF IF5 
  !
!	END IF	IF4
 !
!	WRITE(*,*) 'NEW DT=',DT	
!	
!	CALL LOAD_COORDS

!	END IF	IF2


!	ELSE			  !  IF10
!		                          CALL SAVE_COORDS			! ������� ����
	STEP=STEP+1

	IF(MOD(STEP,10).EQ.0) THEN

	CALL SET_ENVIRS(RSTR,2.0*PLT*SQRT(3.0)/4.0)
	
	WRITE(*,*) 'ENVIRONMENT WAS FOUNDED'

	END IF

	TIME_Rel=TIME_Rel+DT
	WRITE(*,*) 'TIME_Rel=',TIME_Rel
	WRITE(9,100) TIME_REL,EK2,EP2-E0,beta
100		format(	4(e10.3, 2x))
	EK1=EK2
	EP1=EP2
!	SK=SK+1

!	IF(SK.GT.10.AND.DT.LE.0.0051) THEN
!	SK=0
!	DT=2.0*DT
!	WRITE(*,*) 'NEW DT=',DT	 
!	END IF

!	NM=ADD(EP,EP2)
!	NM=ADD(EK,EK2)

	!��������� ���������
	!I6:IF(NM.GT.3.AND.EK(NM-2).LT.EK(NM-1).AND.EK(NM).LT.EK(NM-1))THEN	
!	I6:IF(NM.GT.3)THEN	
    	
!	    if(EK(NM-2).LT.EK(NM-1).AND.EK(NM).LT.EK(NM-1)) then

!	        IF7:IF(ABS((EP2-E0)/E0).LT.0.001) THEN 
        	
!	        WRITE(*,*) 'DELTA E <0.001. CONTINUE RELAX? 0-NO,1-YES'
!	        READ(*,*) CONT
!	        IF(CONT.EQ.0) EXIT      

!	        END IF IF7

!	        CALL NULL_SPEEDS
!	        E0=EP2
!	        WRITE(*,*) '*************SPEEDS ARE NULL***************'

!	        EP1=EP2
!	        EK1=0.0

!	        ALLOCATE(EP(1))
!	        ALLOCATE(EK(1))
!	        EK(1)=EK1
!	        EP(1)=EP1

!	        IF(DT.LT.1E-3) DT=1E-3

!		endif
 !      END IF 	I6

  !
!	END IF IF10	
 !
	END DO

!	DEALLOCATE(EK)
!	DEALLOCATE(EP) 	
	WRITE(1,*) ' Vsego shagov: ', step
	WRITE(1,*) ' Time all: ', TIME_Rel
	WRITE(1,*) ' Epot_end = : ', Ep2
 
	WRITE (*,*) 'POSLE CIKLA'
	CALL GET_RESULT(STRUCT)
	WRITE (*,*) 'POSLE GET_RESULT'	 

	CALL DESTROY_SYSTEM
	WRITE (*,*) 'POSLE DESTROY SYSTEM'

	END SUBROUTINE
